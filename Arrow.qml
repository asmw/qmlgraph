import QtQuick 2.5

Rectangle {
    id: arrow
    property bool bidirectional: false
    property bool endMarker: true

    antialiasing: true
    color: "black"
    height: 2

    Loader {
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        height: parent.height * 4
        width: height
        active: endMarker && bidirectional
        sourceComponent: Triangle {
            rotation: 180
            color: arrow.color
        }
    }

    Loader {
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        height: parent.height * 4
        width: height
        active: endMarker
        sourceComponent: Triangle {
            color: arrow.color
        }
    }
}
