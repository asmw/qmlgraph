import QtQuick 2.0

import org.asmw.qmlgraph 1.0

Line {
    id: edgeDelegate
    property Edge edge: modelData
    property Item fromItem: model.from
    property Item toItem: model.to
    property bool bidirectional: model.bidirectional
    property double weight: width
    onWeightChanged: modelData.weight = weight

    property bool endMarker: true
    property int startOffset: visible ? fromItem.width/2 : 0
    property int endOffset: visible ? toItem.width/2 : 0
    property bool parallelEdges: true
    property int offset: 4
    property color color: "black"

    // GraphNodes use their x + y coordinates as center points
    property int xOffset: !fromItem.graphNode ? fromItem.width/2 : 0
    property int yOffset: !fromItem.graphNode ? fromItem.height/2 : 0

    visible: fromItem && toItem
    from: visible ? Qt.point(fromItem.x + xOffset, fromItem.y + yOffset) : Qt.point(0,0)
    to: visible ? Qt.point(toItem.x + xOffset, toItem.y + yOffset) : Qt.point(0,0)

    Arrow {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: startOffset
        anchors.rightMargin: endOffset
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: edgeDelegate.bidirectional && parallelEdges ? offset : 0
        bidirectional: edgeDelegate.bidirectional && !edgeDelegate.parallelEdges
        endMarker: edgeDelegate.endMarker
        color: edgeDelegate.color
    }

    Loader {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: startOffset
        anchors.rightMargin: endOffset
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: -offset
        rotation: 180
        active: edgeDelegate.bidirectional && edgeDelegate.parallelEdges
        sourceComponent: Arrow {
            endMarker: edgeDelegate.endMarker
            color: edgeDelegate.color
        }
    }
}
