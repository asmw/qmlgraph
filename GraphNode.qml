import QtQuick 2.5

import org.asmw.qmlgraph 1.0

Rectangle {
    id: item
    property Graph graph: parent
    property Item selectedItem
    property Item hoveredItem
    property double mass: 1
    property bool pauseLayout: mouseArea.drag.active
    property bool graphNode: true

    default property alias content: inner.children

    signal selected(Item item)
    signal hovered(Item item)

    color: "transparent"

    Rectangle {
        id: inner
        anchors.horizontalCenter: parent.left
        anchors.verticalCenter: parent.top
        width: parent.width
        height: parent.height
        color: selectedItem === item ? "green" : hoveredItem === item ? "blue" : "grey"
        radius: width

        MouseArea {
            id: mouseArea
            drag.target: item
            anchors.fill: parent
            hoverEnabled: true

            onEntered: hovered(item)
            onExited: hovered(null)

            onDoubleClicked: item.destroy()

            drag.onActiveChanged: if(!drag.active) {
                                      item.graph.layout.updateEdges()
                                  }

            onClicked: {
                if(selectedItem) {
                    if(graph.linked(selectedItem, item)) {
                        graph.unlink(selectedItem, item);
                    } else {
                        graph.link(selectedItem, item);
                    }
                    if(mouse.modifiers & Qt.ShiftModifier) {
                        selected(item);
                    } else {
                        selected(null);
                    }
                } else {
                    selected(item);
                }
            }
        }
    }
}
