import QtQuick 2.0

import org.asmw.qmlgraph 1.0

Graph {
    id: graph
    property Component edgeDelegate: EdgeDelegate {}

    Item {
        id: edgeContainer
        anchors.fill: parent
        Repeater {
            model: graph.edges
            delegate: edgeDelegate
        }
    }
}
