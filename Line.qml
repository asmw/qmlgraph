import QtQuick 2.0

Item {
    property point from: Qt.point(x,y)
    property point to: Qt.point(x,y)

    x: from.x
    y: from.y
    transformOrigin: Item.TopLeft;

    height: 2
    width: Math.sqrt(Math.pow((to.x-from.x),2)+Math.pow((to.y-from.y),2))
    rotation: Math.atan2(to.y - from.y, to.x - from.x) * 180 / Math.PI
}
