import QtQuick 2.0

Canvas {
    id: canvas
    property color color: "black"

    onColorChanged: canvas.requestPaint()

    onPaint: {
        var ctx = getContext("2d")
        ctx.fillStyle = canvas.color
        ctx.beginPath()
        ctx.moveTo(width, height/2)
        ctx.lineTo(0, 0)
        ctx.lineTo(0, height)
        ctx.closePath()
        ctx.fill()
    }
}
