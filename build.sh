#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

mkdir build
cd build
qmake $SCRIPTPATH
make $*
cd ..
mkdir -p org/asmw/qmlgraph
mv build/*.so org/asmw/qmlgraph
cp $SCRIPTPATH/*.qml org/asmw/qmlgraph
cp $SCRIPTPATH/qmldir org/asmw/qmlgraph
echo "run: qml -I . $SCRIPTPATH/test/qmlgraph.qml"
