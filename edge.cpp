#include "edge.h"

using namespace QmlGraph;

Edge::Edge(QQuickItem *from, QQuickItem *to, double weight, double stiffness, QObject *parent) : QObject(parent)
  , m_from(from)
  , m_to(to)
  , m_bidirectional(false)
  , m_weight(weight)
  , m_stiffness(stiffness)
{
    if(from) {
        connect(from, &QObject::destroyed, this, &Edge::doCleanup);
    }
    if(to) {
        connect(to, &QObject::destroyed, this, &Edge::doCleanup);
    }
}

QQuickItem *Edge::from() const
{
    return m_from;
}

QQuickItem *Edge::to() const
{
    return m_to;
}

bool Edge::bidirectional() const
{
    return m_bidirectional;
}

double Edge::length()
{
    return QVector2D(m_from->position() - m_to->position()).length();
}

double Edge::weight() const
{
    return m_weight;
}

double Edge::stiffness() const
{
    return m_stiffness;
}

void Edge::setFrom(QQuickItem *from)
{
    if (m_from == from)
        return;

    if(m_from) {
        disconnect(m_from, &QObject::destroyed, this, &Edge::doCleanup);
    }
    m_from = from;
    connect(m_from, &QObject::destroyed, this, &Edge::doCleanup);
    emit fromChanged(from);
}

void Edge::setTo(QQuickItem *to)
{
    if (m_to == to)
        return;

    if(m_to) {
        disconnect(m_to, &QObject::destroyed, this, &Edge::doCleanup);
    }
    m_to = to;
    connect(m_to, &QObject::destroyed, this, &Edge::doCleanup);
    emit toChanged(to);
}

void Edge::setBidirectional(bool bidirectional)
{
    if (m_bidirectional == bidirectional)
        return;

    m_bidirectional = bidirectional;
    emit bidirectionalChanged(bidirectional);
}

void Edge::doCleanup()
{
    cleanup(this);
}

void Edge::setWeight(double weight)
{
    if (m_weight == weight)
        return;

    m_weight = weight;
    emit weightChanged(weight);
}

void Edge::setStiffness(double stiffness)
{
    if (m_stiffness == stiffness)
        return;

    m_stiffness = stiffness;
    emit stiffnessChanged(stiffness);
}
