#ifndef EDGE_H
#define EDGE_H

#include <QUuid>
#include <QObject>
#include <QQuickItem>

namespace QmlGraph {
class Edge : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQuickItem *from READ from WRITE setFrom NOTIFY fromChanged)
    Q_PROPERTY(QQuickItem *to READ to WRITE setTo NOTIFY toChanged)
    Q_PROPERTY(bool bidirectional READ bidirectional WRITE setBidirectional NOTIFY bidirectionalChanged)
    Q_PROPERTY(double weight READ weight WRITE setWeight NOTIFY weightChanged)
    Q_PROPERTY(double stiffness READ stiffness WRITE setStiffness NOTIFY stiffnessChanged)

public:
    explicit Edge(QQuickItem *from = nullptr, QQuickItem *to = nullptr, double weight = 100, double stiffness = 100, QObject *parent = 0);

    QQuickItem* from() const;
    QQuickItem* to() const;
    bool bidirectional() const;

    Q_INVOKABLE double length();

    double weight() const;

    double stiffness() const;

signals:
    void fromChanged(QQuickItem *from);
    void toChanged(QQuickItem *to);
    void bidirectionalChanged(bool bidirectional);

    void cleanup(Edge* edge);

    void weightChanged(double weight);

    void stiffnessChanged(double stiffness);

public slots:
    void setFrom(QQuickItem *from);
    void setTo(QQuickItem *to);
    void setBidirectional(bool bidirectional);
    void doCleanup();

    void setWeight(double weight);

    void setStiffness(double stiffness);

private:
    QQuickItem *m_from;
    QQuickItem *m_to;
    bool m_bidirectional;
    double m_weight;
    double m_stiffness;
};
}

#endif // EDGE_H
