#include "edge.h"
#include "graph.h"

#include <QDebug>

using namespace QmlGraph;

Graph::Graph(QQuickItem *parent): QQuickItem(parent)
  , m_layout(nullptr)
{
}

Graph::~Graph()
{
}

QQmlListProperty<Edge> Graph::edges()
{
    return QQmlListProperty<Edge>(this,
                                  &m_edges, // data
                                  &Graph::appendEdgeList,
                                  &Graph::countList<Edge>,
                                  &Graph::atList<Edge>,
                                  &Graph::clearList<Edge>);
}

void Graph::link(QQuickItem *from, QQuickItem *to, double weight, double stiffness)
{
    if(from == to) {
        return;
    }
    if(m_edgeMap.contains(ItemPair(from,to))) {
        return;
    }
    if(m_edgeMap.contains(ItemPair(to,from))) {
        m_edgeMap.value(ItemPair(to,from))->setBidirectional(true);
        return;
    }
    if(weight < 0) {
        weight = QVector2D(to->position() - from->position()).length();
    }
    addEdge(new Edge(from, to, weight, stiffness, this));
}

void Graph::unlink(QQuickItem *from, QQuickItem *to)
{
    if(m_edgeMap.contains(ItemPair(from,to))) {
        auto e = m_edgeMap.value(ItemPair(from,to));
        if(e->bidirectional()) {
            addEdge(new Edge(to, from, e->weight(), e->stiffness(), this));
        }
        removeEdge(m_edgeMap.value(ItemPair(from, to)));
    }
    if(m_edgeMap.contains(ItemPair(to,from))) {
        m_edgeMap.value(ItemPair(to,from))->setBidirectional(false);
    }
}

bool Graph::linked(QQuickItem *from, QQuickItem *to)
{
    if(m_edgeMap.contains(ItemPair(from,to))) {
        return true;
    }
    if(m_edgeMap.contains(ItemPair(to,from))) {
        return m_edgeMap.value(ItemPair(to,from))->bidirectional();
    }
    return false;
}

Layout *Graph::layout() const
{
    return m_layout;
}

QVector<Edge *> Graph::edgeVector()
{
    return m_edges;
}

void Graph::addEdge(Edge *edge)
{
    qDebug() << Q_FUNC_INFO;
    connect(edge, &Edge::cleanup, this, &Graph::removeEdge);
    m_edges.append(edge);
    m_edgeMap.insert(ItemPair(edge->from(), edge->to()), edge);
    edgesChanged();
}

void Graph::removeEdge(Edge *edge)
{
    m_edges.removeAll(edge);
    m_edgeMap.remove(ItemPair(edge->from(), edge->to()));
    if(m_edgeMap.contains(ItemPair(edge->to(), edge->from()))) {
        m_edgeMap.value(ItemPair(edge->to(), edge->from()))->setBidirectional(false);
    }
    edgesChanged();
    edge->deleteLater();
}

void Graph::setLayout(Layout *layout)
{
    if (m_layout == layout)
        return;

    m_layout = layout;
    m_layout->setGraph(this);
    emit layoutChanged(layout);
}

void Graph::appendEdgeList(QQmlListProperty<Edge> *list, Edge *edge)
{
    Graph *graph = qobject_cast<Graph *>(list->object);
    if (graph) {
        graph->m_edges.append(edge);
    }
}

void Graph::itemChange(QQuickItem::ItemChange change, const QQuickItem::ItemChangeData &value)
{
    //    if(change == ItemChildAddedChange) {
    //    }
    //    if(change == ItemChildRemovedChange) {
    //    }
    QQuickItem::itemChange(change, value);
}

void Graph::childEvent(QChildEvent *event)
{
    QObject::childEvent(event);
}

template<class T>
T* Graph::atList(QQmlListProperty<T> *list, qsizetype index)
{
    // FIXME: Find a nicer way to do this
    QVector<T*> *v = static_cast<QVector<T*>*>(list->data);
    Graph *graph = qobject_cast<Graph *>(list->object);
    if (graph) {
        if(index < v->count()) {
            return v->at(index);
        }
    }
    return nullptr;
}

template<class T>
void Graph::clearList(QQmlListProperty<T> *list)
{
    // FIXME: Find a nicer way to do this
    QVector<T*> *v = static_cast<QVector<T*>*>(list->data);
    Graph *graph = qobject_cast<Graph *>(list->object);
    if (graph) {
        qDeleteAll(v->begin(), v->end());
    }
}

template<class T>
qsizetype Graph::countList(QQmlListProperty<T> *list)
{
    // FIXME: Find a nicer way to do this
    QVector<T*> *v = static_cast<QVector<T*>*>(list->data);
    Graph *graph = qobject_cast<Graph *>(list->object);
    if (graph) {
        return v->count();
    }
    return -1;
}
