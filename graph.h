#ifndef GRAPH_H
#define GRAPH_H

#include <QQmlListProperty>
#include <QQuickItem>
#include <QVector>
#include <QUuid>

#include "layout.h"

namespace QmlGraph {
class Edge;

class Graph : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(Graph)

    Q_PROPERTY(QQmlListProperty<QmlGraph::Edge> edges READ edges NOTIFY edgesChanged)
    Q_PROPERTY(Layout* layout READ layout WRITE setLayout NOTIFY layoutChanged)

    typedef QPair<QQuickItem*, QQuickItem*> ItemPair;
public:
    Graph(QQuickItem *parent = 0);
    ~Graph();

    bool bidirectional() const;
    QQmlListProperty<Edge> edges();
    QVector<Edge *> edgeVector();
    Layout* layout() const;

    Q_INVOKABLE void link(QQuickItem *from, QQuickItem *to, double weight = -1, double stiffness = 100);
    Q_INVOKABLE void unlink(QQuickItem *from, QQuickItem *to);
    Q_INVOKABLE bool linked(QQuickItem *from, QQuickItem *to);


public slots:
    void addEdge(QmlGraph::Edge *edge);
    void removeEdge(QmlGraph::Edge *edge);
    void setLayout(Layout * layout);

signals:
    void edgesChanged();
    void layoutChanged(Layout * layout);

private:
    QVector<Edge*> m_edges;
    QMap<ItemPair, Edge*> m_edgeMap;
    Layout * m_layout;

    static void appendEdgeList(QQmlListProperty<Edge> *list, Edge *edge);
    template<class T> static T *atList(QQmlListProperty<T> *list, qsizetype index);
    template<class T> static void clearList(QQmlListProperty<T> *list);
    template<class T> static qsizetype countList(QQmlListProperty<T> *list);

protected:
    void itemChange(ItemChange change, const ItemChangeData &value) override;
    void childEvent(QChildEvent *event) override;
};
}

#endif // GRAPH_H

