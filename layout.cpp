#include "layout.h"

#include "edge.h"
#include "graph.h"

#include <QRandomGenerator>

using namespace QmlGraph;

Layout::Layout(Graph *graph,
               double damping,
               double maxSpeed,
               double repulsion,
               double nodeRepulsion, double size,
               double centerForce,
               double centerForceRadius,
               double stiffness,
               double minEnergy,
               double epsilon) : QObject(graph)
  , m_graph(graph)
  , m_damping(damping)
  , m_baseDamping(damping)
  , m_maxSpeed(maxSpeed)
  , m_repulsion(repulsion)
  , m_nodeRepulsion(nodeRepulsion)
  , m_stiffness(stiffness)
  , m_minEnergy(minEnergy)
  , m_epsilon(epsilon)
  , m_energy(0)
  , m_propagateData(false)
  , m_maxLayoutIterations(1000)
  , m_centerForce(centerForce)
  , m_centerForceRadius(centerForceRadius)
  , m_autoDamping(0)
  , m_size(size)
{
}

void Layout::layout()
{
    int x = 0;
    while(x < m_maxLayoutIterations) {
        tick();
        x++;
    }
    applyLayout();
}

void Layout::tick(double timestep)
{
    m_energy = totalEnergy();
    if(m_energy > m_minEnergy) {
        if(m_autoDamping > 0 && m_energy > m_autoDamping) {
            setDamping(m_damping * 0.99);
        } else if(m_damping < m_baseDamping) {
            setDamping(m_baseDamping);
        }
        applyCoulombsLaw();
        applyHookesLaw();
        attractToCenter();
        updateVelocity(timestep);
        updatePosition(timestep);
        m_energy = totalEnergy();
    }
    energyChanged(m_energy);
}

void Layout::eachNode(std::function<void(QQuickItem *node, LayoutPoint *point)> visitor) const
{
    foreach(QQuickItem *node, m_layoutMap.keys()) {
        visitor(node, m_layoutMap[node]);
    }
}

void Layout::eachSpring(std::function<void(LayoutSpring *spring)> visitor) const
{
    foreach(LayoutSpring *spring, m_springMap.values()) {
        visitor(spring);
    }
}

Graph *Layout::graph() const
{
    return m_graph;
}

double Layout::damping() const
{
    return m_damping;
}

double Layout::maxSpeed() const
{
    return m_maxSpeed;
}

double Layout::repulsion() const
{
    return m_repulsion;
}

double Layout::stiffness() const
{
    return m_stiffness;
}

double Layout::minEnergy() const
{
    return m_minEnergy;
}

double Layout::energy() const
{
    return m_energy;
}

QPoint Layout::center() const
{
    return m_center.toPoint();
}

double Layout::epsilon() const
{
    return m_epsilon;
}

bool Layout::propagateData() const
{
    return m_propagateData;
}

void Layout::setGraph(Graph *graph)
{
    qDebug() << Q_FUNC_INFO << graph;
    if (m_graph == graph)
        return;

    if(m_graph) {
        disconnect(m_graph, &Graph::edgesChanged, this, &Layout::updateEdges);
        disconnect(m_graph, &Graph::widthChanged, this, &Layout::updateCenter);
        disconnect(m_graph, &Graph::heightChanged, this, &Layout::updateCenter);
    }
    cleanup();
    m_graph = graph;
    setup();
    qDebug() << Q_FUNC_INFO;
    connect(m_graph, &Graph::edgesChanged, this, &Layout::updateEdges);
    connect(m_graph, &Graph::widthChanged, this, &Layout::updateCenter);
    connect(m_graph, &Graph::heightChanged, this, &Layout::updateCenter);
    emit graphChanged(graph);
    qDebug() << Q_FUNC_INFO;
}

void Layout::setDamping(double damping)
{
    if (m_damping == damping)
        return;

    m_damping = damping;
    emit dampingChanged(damping);
}

void Layout::setMaxSpeed(double maxSpeed)
{
    if (m_maxSpeed == maxSpeed)
        return;

    m_maxSpeed = maxSpeed;
    emit maxSpeedChanged(maxSpeed);
}

void Layout::setRepulsion(double repulsion)
{
    if (m_repulsion == repulsion)
        return;

    m_repulsion = repulsion;
    emit repulsionChanged(repulsion);
}

void Layout::setStiffness(double stiffness)
{
    if (m_stiffness == stiffness)
        return;

    m_stiffness = stiffness;
    eachSpring([stiffness](LayoutSpring *spring){
        spring->m_stiffness = stiffness;
    });
    emit stiffnessChanged(stiffness);
}

void Layout::setMinEnergy(double minEnergy)
{
    if (m_minEnergy == minEnergy)
        return;

    m_minEnergy = minEnergy;
    emit minEnergyChanged(minEnergy);
}

void Layout::updateEdges()
{
    if(!m_graph) return;
    // FIXME: Improve to remove this cleanup
    cleanup();
    foreach(Edge *e, m_graph->edgeVector()) {
        if(!e->from() || !e->to()) return;
        if(!m_layoutMap.contains(e->from())) {
            m_layoutMap.insert(e->from(), layoutPointFromItem(e->from()));
        } else {
            m_layoutMap[e->from()]->m_position = QVector2D(e->from()->position());
        }
        if(!m_layoutMap.contains(e->to())) {
            m_layoutMap.insert(e->to(), layoutPointFromItem(e->to()));
        } else {
            m_layoutMap[e->to()]->m_position = QVector2D(e->to()->position());
        }

        if(!m_springMap.contains(ItemPair(e->from(), e->to()))) {
            LayoutSpring *spring = new LayoutSpring();
            spring->m_stiffness = e->stiffness();
            spring->m_length = e->weight();
            spring->m_point1 = m_layoutMap[e->from()];
            spring->m_point2 = m_layoutMap[e->to()];
            m_springMap.insert(ItemPair(e->from(), e->to()), spring);
        } else {
            m_springMap[ItemPair(e->from(), e->to())]->m_length = e->weight();
        }
    }
    qDebug() << Q_FUNC_INFO;
    m_energy = m_minEnergy + 1;
    energyChanged(m_energy);
}

void Layout::updateCenter()
{
    m_center = QVector2D(m_graph->boundingRect().center());
}

void Layout::updatePositions()
{
    foreach(Edge *e, m_graph->edgeVector()) {
        if(m_layoutMap.contains(e->from())) {
            m_layoutMap[e->from()]->m_position = QVector2D(e->from()->position());
        }
        if(m_layoutMap.contains(e->to())) {
            m_layoutMap[e->to()]->m_position = QVector2D(e->to()->position());
        }
    }
}

void Layout::setNodeRepulsion(double nodeRepulsion)
{
    if (m_nodeRepulsion == nodeRepulsion)
            return;

        m_nodeRepulsion = nodeRepulsion;
        emit nodeRepulsionChanged(nodeRepulsion);
}

void Layout::setSize(double size)
{
    if (m_size == size)
            return;

        m_size = size;
        emit sizeChanged(size);
}

void Layout::setAutoDamping(double autoDamping)
{
    if (m_autoDamping == autoDamping)
        return;

    m_autoDamping = autoDamping;
    emit autoDampingChanged(autoDamping);
}

double Layout::nodeRepulsion() const
{
    return m_nodeRepulsion;
}

double Layout::size() const
{
    return m_size;
}

void Layout::setCenterForce(double centerForce)
{
    if (m_centerForce == centerForce)
        return;

    m_centerForce = centerForce;
    emit centerForceChanged(centerForce);
}

void Layout::setCenterForceRadius(double centerForceRadius)
{
    if (m_centerForceRadius == centerForceRadius)
        return;

    m_centerForceRadius = centerForceRadius;
    emit centerForceRadiusChanged(centerForceRadius);
}

double Layout::autoDamping() const
{
    return m_autoDamping;
}

void Layout::setMaxLayoutIterations(int maxLayoutIterations)
{
    if (m_maxLayoutIterations == maxLayoutIterations)
        return;

    m_maxLayoutIterations = maxLayoutIterations;
    emit maxLayoutIterationsChanged(maxLayoutIterations);
}

double Layout::centerForce() const
{
    return m_centerForce;
}

double Layout::centerForceRadius() const
{
    return m_centerForceRadius;
}

void Layout::setEpsilon(double epsilon)
{
    if (m_epsilon == epsilon)
        return;

    m_epsilon = epsilon;
    emit epsilonChanged(epsilon);
}

void Layout::setPropagateData(bool propagateData)
{
    if (m_propagateData == propagateData)
        return;

    m_propagateData = propagateData;
    emit propagateDataChanged(propagateData);
}

int Layout::maxLayoutIterations() const
{
    return m_maxLayoutIterations;
}

void Layout::setCenter(QPoint center)
{
    QVector2D tmp = QVector2D(center);
    if (m_center == tmp)
        return;

    m_center = tmp;
    emit centerChanged(center);
}

void Layout::applyCoulombsLaw()
{
    eachNode([this](QQuickItem *node1, LayoutPoint *point1) {
        Q_UNUSED(node1);
        eachNode([this, point1](QQuickItem *node2, LayoutPoint *point2) {
            Q_UNUSED(node2);
            if (point1 != point2)
            {
                QVector2D d = point1->m_position - point2->m_position;
                double distance = (d.length()/m_size) + m_epsilon; // avoid massive forces at small distances (and divide by zero)
                QVector2D direction = d.normalized();
                point1->applyForce((direction * m_nodeRepulsion) / (distance * distance * 0.5));
                point2->applyForce((direction * m_nodeRepulsion) / (distance * distance * -0.5));
            }
        });
    });
}

void Layout::applyHookesLaw()
{
    eachSpring([this](LayoutSpring *spring){
        QVector2D d = spring->m_point2->m_position - spring->m_point1->m_position; // the direction of the spring
        double displacement = spring->m_length - d.length();
        QVector2D direction = d.normalized();

        // apply force to each end point
        spring->m_point1->applyForce(direction * m_repulsion * (spring->m_stiffness * displacement * -0.5));
        spring->m_point2->applyForce(direction * m_repulsion * (spring->m_stiffness * displacement * 0.5));
    });
}

void Layout::attractToCenter()
{
    eachNode([this](QQuickItem *node, LayoutPoint *point) {
        Q_UNUSED(node);
        double power = (point->m_position - m_center).length();
        if(power < m_centerForceRadius) {
            return;
        }
        QVector2D direction = (m_center + (point->m_position * -1)).normalized();
        point->applyForce(direction * power * m_centerForce);
    });
}

void Layout::updateVelocity(double timestep)
{
    eachNode([this, timestep](QQuickItem *node, LayoutPoint *point) {
        Q_UNUSED(node);
        point->m_velocity = (point->m_velocity + (point->m_acceleration * timestep)) * m_damping;
        if (point->m_velocity.length() > m_maxSpeed) {
            point->m_velocity = point->m_velocity.normalized() * m_maxSpeed;
        }
        point->m_acceleration = {0,0};
    });
}

void Layout::updatePosition(double timestep)
{
    //    qDebug() << Q_FUNC_INFO << timestep;
    eachNode([timestep](QQuickItem *node, LayoutPoint *point) {
        Q_UNUSED(node);
        point->m_position = point->m_position + (point->m_velocity * timestep);
    });
}

double Layout::totalEnergy() const
{
    double energy = 0;
    eachNode([&energy](QQuickItem *node, LayoutPoint *point) {
        Q_UNUSED(node);
        double speed = point->m_velocity.length();
        energy += point->m_mass * speed * speed;
    });
    return energy;
}

void Layout::setup()
{
    qDebug() << Q_FUNC_INFO;
    if(!m_graph) return;
    m_center = QVector2D(m_graph->boundingRect().center());
    updateEdges();
}

void Layout::cleanup() {
    qDebug() << Q_FUNC_INFO;
    qDeleteAll(m_layoutMap.values());
    m_layoutMap.clear();
    qDeleteAll(m_springMap.values());
    m_springMap.clear();
}

void Layout::updateNode(QQuickItem *n, LayoutPoint *p)
{
    n->setProperty("x", p->m_position.x());
    n->setProperty("y", p->m_position.y());
    if(m_propagateData) {
        n->setProperty("velocity", p->m_velocity);
    }
}

void Layout::applyLayout()
{
    eachNode([this](QQuickItem *node, LayoutPoint *p){
        if(!node->property("pauseLayout").toBool()) {
            updateNode(node, p);
        } else {
            // Set the layoutpoint to the node coordinates
            p->m_position = QVector2D(node->position());
        }
    });
}

LayoutPoint *Layout::layoutPointFromItem(QQuickItem *item)
{
    LayoutPoint *p = new LayoutPoint();
    bool ok;
    double mass = item->property("mass").toDouble(&ok);
    if(ok) {
        p->m_mass = mass;
    }
    p->m_velocity = randomVector();
    p->m_position = QVector2D(item->position());
    return p;
}

QVector2D Layout::randomVector()
{
    return QVector2D(QRandomGenerator::global()->generateDouble(), QRandomGenerator::global()->generateDouble()).normalized();
}

