#ifndef LAYOUT_H
#define LAYOUT_H

#include <functional>

#include <QVector>
#include <QVector2D>
#include <QHash>
#include <QPair>

#include <QDebug>

class QQuickItem;

namespace QmlGraph {
class Graph;

typedef QPair<QQuickItem*,QQuickItem*> ItemPair;

struct LayoutPoint {
    double m_mass = 1;
    QVector2D m_position;
    QVector2D m_velocity;
    QVector2D m_acceleration;

    void applyForce(QVector2D force) {
        m_acceleration += (force / m_mass);
    }
};

struct LayoutSpring {
    LayoutPoint *m_point1 = nullptr;
    LayoutPoint *m_point2 = nullptr;
    double m_stiffness = 0.5;
    double m_length = 1.0;
};

class Layout : public QObject
{
    Q_OBJECT

    Q_PROPERTY(double damping READ damping WRITE setDamping NOTIFY dampingChanged)
    Q_PROPERTY(double maxSpeed READ maxSpeed WRITE setMaxSpeed NOTIFY maxSpeedChanged)
    Q_PROPERTY(double repulsion READ repulsion WRITE setRepulsion NOTIFY repulsionChanged)
    Q_PROPERTY(double nodeRepulsion READ nodeRepulsion WRITE setNodeRepulsion NOTIFY nodeRepulsionChanged)
    Q_PROPERTY(double centerForce READ centerForce WRITE setCenterForce NOTIFY centerForceChanged)
    Q_PROPERTY(double centerForceRadius READ centerForceRadius WRITE setCenterForceRadius NOTIFY centerForceRadiusChanged)
    Q_PROPERTY(double size READ size WRITE setSize NOTIFY sizeChanged) // Size of the "universe" in pixels, used to determine repulsive forces between nodes
    Q_PROPERTY(double stiffness READ stiffness WRITE setStiffness NOTIFY stiffnessChanged)
    Q_PROPERTY(double minEnergy READ minEnergy WRITE setMinEnergy NOTIFY minEnergyChanged)
    Q_PROPERTY(double energy READ energy NOTIFY energyChanged)
    Q_PROPERTY(double epsilon READ epsilon WRITE setEpsilon NOTIFY epsilonChanged)
    Q_PROPERTY(QPoint center READ center WRITE setCenter NOTIFY centerChanged)
    Q_PROPERTY(bool propagateData READ propagateData WRITE setPropagateData NOTIFY propagateDataChanged)
    Q_PROPERTY(int maxLayoutIterations READ maxLayoutIterations WRITE setMaxLayoutIterations NOTIFY maxLayoutIterationsChanged)
    Q_PROPERTY(double autoDamping READ autoDamping WRITE setAutoDamping NOTIFY autoDampingChanged)

public:
    Layout(Graph *graph = nullptr,
           double damping = 0.9,
           double maxSpeed = 10000,
           double repulsion = 10,
           double nodeRepulsion = 100,
           double size = 400,
           double centerForce = 1,
           double centerForceRadius = 0,
           double stiffness = 10,
           double minEnergy = 0.1,
           double epsilon = 0.1);

    Q_INVOKABLE void layout();
    Q_INVOKABLE void tick(double timestep = 0.03);
    Q_INVOKABLE void applyLayout();

    void setup();
    void cleanup();

    void eachNode(std::function<void(QQuickItem* node, LayoutPoint* point)> visitor) const;
    void eachSpring(std::function<void(LayoutSpring* point)> visitor) const;

    Graph* graph() const;
    double damping() const;
    double maxSpeed() const;
    double repulsion() const;
    double stiffness() const;
    double minEnergy() const;
    double energy() const;
    QPoint center() const;
    double epsilon() const;
    bool propagateData() const;
    int maxLayoutIterations() const;
    double centerForce() const;
    double centerForceRadius() const;
    double autoDamping() const;
    double nodeRepulsion() const;
    double size() const;

    void setGraph(Graph* graph);
    void setDamping(double damping);
    void setMaxSpeed(double maxSpeed);
    void setRepulsion(double repulsion);
    void setStiffness(double stiffness);
    void setMinEnergy(double minEnergy);
    void setCenter(QPoint center);
    void setEpsilon(double epsilon);
    void setPropagateData(bool propagateData);
    void setMaxLayoutIterations(int maxLayoutIterations);
    void setCenterForce(double centerForce);
    void setCenterForceRadius(double centerForceRadius);
    void setNodeRepulsion(double nodeRepulsion);
    void setAutoDamping(double autoDamping);
    void setSize(double size);

public slots:
    void updateEdges();
    void updateCenter();
    void updatePositions();

signals:
    void graphChanged(Graph* graph);
    void dampingChanged(double damping);
    void maxSpeedChanged(double maxSpeed);
    void repulsionChanged(double repulsion);
    void stiffnessChanged(double stiffness);
    void minEnergyChanged(double minEnergy);
    void energyChanged(double energy);
    void centerChanged(QPoint center);
    void epsilonChanged(double epsilon);
    void propagateDataChanged(bool propagateData);
    void maxLayoutIterationsChanged(int maxLayoutIterations);
    void centerForceChanged(double centerForce);
    void centerForceRadiusChanged(double centerForceRadius);
    void autoDampingChanged(double autoDamping);
    void nodeRepulsionChanged(double nodeRepulsion);
    void sizeChanged(double size);

protected:
    void applyCoulombsLaw();
    void applyHookesLaw();
    void attractToCenter();
    void updateVelocity(double timestep);
    void updatePosition(double timestep);
    double totalEnergy() const;
    void updateNode(QQuickItem *n, LayoutPoint *p);

private:
    QHash<QQuickItem*, LayoutPoint*> m_layoutMap;
    QHash<ItemPair, LayoutSpring*> m_springMap;

    Graph* m_graph = nullptr;
    QVector2D m_center;

    double m_damping;
    double m_baseDamping;
    double m_maxSpeed;
    double m_repulsion;
    double m_nodeRepulsion;
    double m_stiffness;
    double m_minEnergy;
    double m_epsilon;
    double m_energy;
    bool m_propagateData;
    int m_maxLayoutIterations;
    double m_centerForce;
    double m_centerForceRadius;
    double m_autoDamping;
    double m_size;

    LayoutPoint *layoutPointFromItem(QQuickItem *item);
    QVector2D randomVector();
};
}

#endif // LAYOUT_H
