TEMPLATE = lib
TARGET = qmlgraph
QT += qml quick
CONFIG += plugin c++11
QMAKE_CXXFLAGS += -Wall

TARGET = qmlgraph
uri = org.asmw.qmlgraph

# Input
SOURCES += \
    qmlgraph_plugin.cpp \
    graph.cpp \
    edge.cpp \
    layout.cpp

HEADERS += \
    qmlgraph_plugin.h \
    graph.h \
    edge.h \
    layout.h

DISTFILES = qmldir \
    Line.qml \
    GraphView.qml \
    qmlgraph.qml \
    Triangle.qml \
    Arrow.qml \
    EdgeDelegate.qml \
    test/layout_test.qml \
    GraphNode.qml

OTHER_FILES += test/*

distfiles.files = $$DISTFILES
target.path += $$[QT_INSTALL_QML]/org/asmw/qmlgraph/
distfiles.path += $$[QT_INSTALL_QML]/org/asmw/qmlgraph/

INSTALLS += target distfiles

RESOURCES +=
