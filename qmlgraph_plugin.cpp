#include "qmlgraph_plugin.h"
#include "graph.h"
#include "edge.h"
#include "layout.h"

#include <qqml.h>

using namespace QmlGraph;

void QmlgraphPlugin::registerTypes(const char *uri)
{
    // @uri org.asmw.qmlgraph
    qmlRegisterType<Edge>(uri, 1, 0, "Edge");
    qmlRegisterType<Graph>(uri, 1, 0, "Graph");
    qmlRegisterType<Layout>(uri, 1, 0, "GraphLayout");
}

