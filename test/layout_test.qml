import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.asmw.qmlgraph 1.0

Window {
    id: root
    width: 1200
    height: 900
    visible: true

    property Item selectedItem
    property Item hoveredItem
    property bool bidirectional: true

    property var created: ([])

    property int nodeCount: 20

    Timer {
        id: layoutTimer
        interval: 1000/60
        running: layoutActive.checked

        onRunningChanged: if(running) {
                              graphView.layout.updateEdges();
                          }

        onTriggered: {
            graphView.layout.tick();
            graphView.layout.applyLayout();
        }
        repeat: true
    }

    function randomConnect(node) {
        var again = 1;
        while(again > 0.5) {
            var t = created[randInt(0, created.length-1)]
            while(node === t) {
                t = created[randInt(0, created.length-1)]
            }
            graphView.link(node, t);
            again = again - Math.random();
        }
    }

    function randInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function createNode(x, y) {
        var size = randInt(10,30);
        var c = spawnComponent.createObject(graphView, {"x": x - size/2, "y": y - size/2, "width": size, "height": size});
        root.created.push(c);
    }

    Component.onCompleted: {
        for(var i = 0; i < nodeCount; i++) {
            createNode(root.randInt(50,graphView.width - 50), root.randInt(50,graphView.height - 50));
        }
        for(var x = 0; x < created.length; x++) {
            randomConnect(created[x]);
        }
        graphView.layout.updateEdges();
    }

    Component {
        id: spawnComponent
        GraphNode {
            property double mass: 1 + width/root.width
        }
    }

    GraphView {
        id: graphView
        anchors.fill: parent
        layout: GraphLayout {
            center: center.center
            centerForceRadius: 200
            autoDamping: 20000
        }

//        edges: [
//            Edge { from: one; to: zero },
//            Edge { from: one; to: two },
//            Edge { from: two; to: three },
//            Edge { from: three; to: one }
//        ]

//        GraphNode {
//            id: zero
//            objectName: "0"
//            x: root.width/2
//            y: root.height/2 - 100
//            width: 10
//            height: width
//            Text { text: parent.objectName ; anchors.centerIn: parent; font.pointSize: 22}
//        }
//        GraphNode {
//            id: one
//            objectName: "1"
//            x: root.width/2
//            y: root.height/2 - 50
//            width: 10
//            height: width
//            Text { text: parent.objectName ; anchors.centerIn: parent}
//        }
//        GraphNode {
//            id: two
//            objectName: "2"
//            x: root.width/2 - 50
//            y: root.height/2 + 50
//            width: 10
//            height: width
//            Text { text: parent.objectName ; anchors.centerIn: parent}
//        }
//        GraphNode {
//            id: three
//            objectName: "3"
//            x: root.width/2 + 50
//            y: root.height/2 + 50
//            width: 10
//            height: width
//            Text { text: parent.objectName ; anchors.centerIn: parent}
//        }
    }

    Item {
        id: center

        property point center: Qt.point(x,y)

        x: parent.width/2 - width/2
        y: parent.height/2 - height/2
        width: 1
        height: 1
        Rectangle {
            anchors.centerIn: parent
            width: 30
            height: width
            radius: width
            color: "lightgreen"
            z: 100
            MouseArea {
                anchors.fill: parent
                drag.target: center
            }
        }


        Rectangle {
            anchors.centerIn: parent
            width: graphView.layout.centerForceRadius * 2
            height: width
            radius: width
            color: "transparent"
            border.color: "red"

            Behavior on width { NumberAnimation {} }
        }
    }

    ColumnLayout {
        anchors.top: parent.top
        Label {
            text: "Energy: " + graphView.layout.energy
        }

    }

    ColumnLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        RowLayout {
            spacing: 5
            Button {
                text: "Tick"
                onClicked: {
                    graphView.layout.updateEdges();
                    graphView.layout.tick();
                    graphView.layout.applyLayout();
                }
            }

            Switch {
                id: layoutActive
                text: "Layout"
                checked: false
            }
        }
        RowLayout {
            Layout.fillWidth: true
            Label {text: "Stiffness"}
            TextField {
                Layout.fillWidth: true
                text: graphView.layout.stiffness
                onAccepted: graphView.layout.stiffness = text
            }
            Label {text: "Damping"}
            TextField {
                Layout.fillWidth: true
                text: graphView.layout.damping
                onAccepted: graphView.layout.damping = text
            }
            Label {text: "Spring Repulsion"}
            TextField {
                Layout.fillWidth: true
                text: graphView.layout.repulsion
                onAccepted: graphView.layout.repulsion = text
            }
            Label {text: "Node Repulsion"}
            TextField {
                Layout.fillWidth: true
                text: graphView.layout.nodeRepulsion
                onAccepted: graphView.layout.nodeRepulsion = text
            }
            Label {text: "Center force"}
            TextField {
                Layout.fillWidth: true
                text: graphView.layout.centerForce
                onAccepted: graphView.layout.centerForce = text
            }
            Label {text: "Center force radius"}
            TextField {
                Layout.fillWidth: true
                text: graphView.layout.centerForceRadius
                onAccepted: graphView.layout.centerForceRadius = text
            }
            Label {text: "MaxSpeed"}
            TextField {
                Layout.fillWidth: true
                text: graphView.layout.maxSpeed
                onAccepted: graphView.layout.maxSpeed = text
            }
            Label {text: "MinEnergy"}
            TextField {
                Layout.fillWidth: true
                text: graphView.layout.minEnergy
                onAccepted: graphView.layout.minEnergy = text
            }
            Label {text: "Epsilon"}
            TextField {
                Layout.fillWidth: true
                text: graphView.layout.epsilon
                onAccepted: graphView.layout.epsilon = text
            }
        }
    }
}
