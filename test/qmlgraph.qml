import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import org.asmw.qmlgraph 1.0

Window {
    id: root
    width: 600
    height: 600
    visible: true

    property Item selectedItem
    property Item hoveredItem
    property bool bidirectional: bidiSwitch.checked

    function randInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function createNode(x, y) {
        var size = randInt(20,50);
        spawnComponent.createObject(graphView, {"x": x - size/2, "y": y - size/2, "width": size, "height": size});
    }

    Component.onCompleted: {
        for(var i = 0; i < 10; i++) {
            createNode(root.randInt(50,graphView.width - 50), root.randInt(50,graphView.height - 50));
        }
    }

    Component {
        id: spawnComponent
        GraphNode {
            selectedItem: root.selectedItem
            hoveredItem: root.hoveredItem
            graph: graphView

            onSelected: root.selectedItem = item
            onHovered: root.hoveredItem = item
        }
    }

    GraphView {
        id: graphView
        anchors.fill: parent

        Behavior on scale {
            NumberAnimation {}
        }

        edges: [Edge{from: gr; to: mg; bidirectional: true}]
        layout: GraphLayout {
            repulsion: 10
            stiffness: 10
            size: 400
            nodeRepulsion: 20
            minEnergy: 0.1
            centerForce: 10
            centerForceRadius: 0
            maxLayoutIterations: 10000
        }

        edgeDelegate: EdgeDelegate {
            parallelEdges: root.bidirectional
            color: linkMouseArea.containsMouse ? "red" : "black"
            MouseArea {
                id: linkMouseArea
                anchors.centerIn: parent
                width: parent.width
                height: 10
                hoverEnabled: true

                onDoubleClicked: graphView.removeEdge(modelData)

                onWheel: {
                    weight += wheel.angleDelta.y/12;
                    graphView.layout.updateEdges();
                    graphView.layout.layout();
                }
            }
        }

        Rectangle {
            id: gr
            color: "green"

            x: 100
            y: 100
            width: 30
            height: 30
        }

        Rectangle {
            id: mg
            color: "magenta"

            x: 300
            y: 300
            width: 30
            height: 30
        }
        Loader {
            active: selectedItem
            sourceComponent: Line {
                from: selectedItem ? Qt.point(root.selectedItem.x, root.selectedItem.y) : Qt.point(0,0)
                to: root.hoveredItem ? Qt.point(root.hoveredItem.x, root.hoveredItem.y) :
                                       mouseArea.mapToItem(graphView, mouseArea.mouseX, mouseArea.mouseY)
                Rectangle {
                    anchors.fill: parent
                    color: "black"
                    antialiasing: true
                }
            }
        }
    }

    Text {
        z: -1
        text: "<ul>
               <li>Right click background to add nodes</li>
               <li>Click nodes to select</li>
               <li>Shift-Click to chain connections</li>
               <li>Click second node while selected to toggle link</li>
               <li>Doubleclick node to delete</li>
               <li>Drag nodes to move</li>
               </ul>"
    }

    MouseArea {
        z: -1
        id: mouseArea
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton

        hoverEnabled: root.selectedItem

        onClicked: {
            if(root.selectedItem) {
                selectedItem = null
            } else if(mouse.button === Qt.RightButton) {
                var p = mouseArea.mapToItem(graphView, mouseArea.mouseX, mouseArea.mouseY);
                createNode(p.x, p.y);
            }
        }

        onWheel: {
            graphView.scale += wheel.angleDelta.y/1200
        }
    }

    Row {
        anchors.bottom: parent.bottom
        spacing: 5
        Button {
            text: "Layout"
            onClicked: {
                graphView.layout.updateEdges();
                graphView.layout.layout();
            }
        }

        Switch {
            id: bidiSwitch
            text: "Parallel edges" + bidirectional
        }
        Label {
            anchors.verticalCenter: parent.verticalCenter
            text: "Selected node:" + (root.selectedItem ? root.selectedItem : "-")
        }
    }
}
